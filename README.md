Javkovsky
=========

Basic music composer using the JFugue library to synthesize notes, and able to execute music via beep on linux platforms.
Compatible with Windows/OSX/Linux.  
Can load and save music as executable .sh scripts.  
Programmed in Java.  

2013 © Philippe Jaleev  
Code available under GPLv3 License -- see LICENSE file or visit http://opensource.org/licenses/GPL-3.0
