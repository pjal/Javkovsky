/*
Javkovsky
2014 Philippe Jaleev
This code is made available under a GPLv3 License -- http://opensource.org/licenses/GPL-3.0
*/

package src.javkovsky;

import java.awt.BorderLayout;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.sound.midi.MidiUnavailableException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jfugue.realtime.RealTimePlayer;

public class Application {
	static String notes = "";
	static JFrame winReader = new JFrame();
	static boolean readInit = false;
	static final JTextArea text = new JTextArea(notes);
	static boolean mousePressed = false;
	static int counter = 0;
	static long startTime;
	static long elapsedTime;
	static String instrument = "Piano";
	static int oct = 3;
	static String note;
	static String foo;
	static RealTimePlayer player;
	
	public static void main(String[] args) {
		try {
			player = new RealTimePlayer();
		} catch (MidiUnavailableException e1) {
			e1.printStackTrace();
		}
		JFrame window = new JFrame();
		window.setLayout(new BorderLayout());
		JPanel upper = new JPanel();
		upper.setLayout(new GridLayout(1,21));
		JPanel lower = new JPanel();
		lower.setLayout(new GridLayout(1,21));
		JPanel controls = new JPanel();
		controls.setLayout(new FlowLayout());
		JPanel reader = new JPanel();
		reader.add(new JTextField("test"));
		JButton[][] keys = new JButton[21][2];
		for(int j=0;j<42;j++){
			final int i = j;
			if(j<21){
				if(j%7!=0 && j%7!=3){
					keys[j][0] = newButton(i);
					upper.add(keys[j][0]);
				}else{
					upper.add(new JPanel());
				}
			}else{
				keys[j-21][1] =newButton(i);					               
				lower.add(keys[j-21][1]);	
			}
		}
		String[] oList = {"1","2","3","4","5"};
		final JComboBox octaveList = new JComboBox(oList);
		octaveList.setSelectedIndex(2);
		octaveList.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		oct = Integer.valueOf((String) octaveList.getSelectedItem());
        	}
		});
		String[] iList = { "Piano", "Bright_Acoustic", "Electric_Grand", "Honkey_Tonk", "Electric_Piano", "Electric_Piano2", "Harpischord","Clavinet","Drawbar_Organ","Percussive_Organ","Rock_Organ","Church_Organ", "Reed_Organ","Accordian","Harmonica","Tango_Accordian","Acoustic_Bass","Electric_Bass_Finger","Electric_Bass_Pick","Fretless_Bass","Slap_Bass_1","Slap_Bass_2","Synth_Bass_1","Synth_Bass_2","String_Ensemble_1","String_Ensemble_2","Synth_Strings_1","Synth_Strings_2","Choir_Aahs","Voice_Oohs","Synth_Voice","Orchestra_Hit","Celesta","Glockenspiel","Music_Box","Vibraphone","Marimba","Xylophone","Tubular_Bells","Dulcimer","Guitar","Steel_String_Guitar","Electric_Jazz_Guitar","Electric_Clean_Guitar","Electric_Muted_Guitar","Overdriven_Guitar","Distortion_Guitar","Guitar_Harmonics","Violin","Viola","Cello","Contrabass","Tremolo_Strings","Pizzicato_Strings","Orchestral_Strings","Timpani","Trumpet","Trombone","Tuba","Muted_Trumpet","French_Horn","Brass_Section","Synthbrass_1","Synthbrass_2","Soprano_Sax","Alto_Sax","Tenor_Sax","Baritone_Sax","Oboe","English_Horn","Bassoon","Clarinet","Lead_Square","Lead_Sawtooth","Lead_Calliope", "Lead_Chiff","Lead_Charang","Lead_Voice","Lead_Fifths","Lead_Basslead","Rain","Soundtrack","Atmosphere","Brightness","Goblins","Echoes","Sci-Fi","Tinkle_Bell","Agogo","Steel_Drums","Woodblock","Taiko_Drum","Melodic_Tom","Synth_Drum","Reverse_Cymbal","Piccolo","Flute","Recorder","Pan_Flute","Blown_Bottle","Skakuhachi","Whistle","Ocarina","Pad_New_Age","Pad_Warm","Pad_Polysynth","Pad_Choir","Pad_Bowed","Pad_Metallic","Pad_Halo","Pad_Sweep","Sitar","Banjo","Shamisen","Koto","Kalimba","Bagpipe","Fiddle","Shanai","Guitar_Fret_Noise","Breath_Noise","Seashore","Bird_Tweet","Telephone_Ring","Helicopter","Applause","Gunshot"};
		final JComboBox jciList = new JComboBox(iList);
		jciList.setSelectedIndex(0);
		jciList.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        //		instrument = (String) jciList.getSelectedItem();
        		player.changeInstrument(jciList.getSelectedIndex());
        	}
		});
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		System.exit(0);
        	}
		});
		JButton btnReader = new JButton("Reader");
		btnReader.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		if(readInit)
        			winReader.setVisible(true);
        		else
        			read();
        	}
		});
		JButton btnPause = new JButton("Pause");
		btnPause.addMouseListener(new MouseAdapter() {
	        public void mousePressed(MouseEvent e) {
	            new Thread() {
	                public void run() {
	                	startTime = System.currentTimeMillis();
	                }

	            }.start();
	        }
	        public void mouseReleased(MouseEvent e) {
	            elapsedTime = System.currentTimeMillis() - startTime;
	            notes += "sleep " + elapsedTime + "\n";
        		text.setText(notes);
	        }

	    });
		controls.add(octaveList);
		controls.add(jciList);
		controls.add(exit);
		controls.add(btnReader);
		controls.add(btnPause);
		window.add(upper, BorderLayout.NORTH);
		window.add(lower,BorderLayout.CENTER);
		window.add(controls,BorderLayout.SOUTH);
		window.setSize(1300,300);
		window.setResizable(false);
		window.setTitle("Javkovsky");
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static String getKey(int i){
		if(i<21){
			switch(i%7){
				case 0:return "B#";
				case 1:return "C#";
				case 2:return "D#";
				case 3:return "E#";
				case 4:return "F#";
				case 5:return "G#";
				case 6:return "A#";
			}
		}else{
			switch(i%7){
				case 0:return "C";
				case 1:return "D";
				case 2:return "E";
				case 3:return "F";
				case 4:return "G";
				case 5:return "A";
				case 6:return "B";
			}
		}
		return "error";
	}
	
	public static JButton newButton(final int i){
		JButton a = new JButton(getKey(i));
		 a.addMouseListener(new MouseAdapter() {
		        public void mousePressed(MouseEvent e) {
		            new Thread() {
		                public void run() {
		                	startTime = System.currentTimeMillis();
		                	if(i<21){
		                		note = getKey(i)+(oct+(i+21)/7-3);
		                		foo = getKey(i)+(oct+(i+21)/7-4);
		                	}else{
		                		note = getKey(i)+(oct+i/7-3);
		                		foo = getKey(i)+(oct+i/7-4);
		                	}
		                	player.play(foo);
		                }

		            }.start();
		        }
		        public void mouseReleased(MouseEvent e) {
		        	player.play(foo+"-s");
		        	elapsedTime = System.currentTimeMillis() - startTime;
		            double freq = getFreq(note);
		            notes += "beep -f " + freq + " -l " + elapsedTime + "\n";
	        		text.setText(notes);
		        }

		    });
		 return a;
	}
	
	protected static double getFreq(String bar) {
		int i = 0;
		switch(bar.charAt(0)){
			case 'C':i=0;break;
			case 'D':i=2;break;
			case 'E':i=4;break;
			case 'F':i=5;break;
			case 'G':i=7;break;
			case 'A':i=9;break;
			case 'B':i=11;break;
		}
		if(bar.charAt(1)=='#'){
			i++;
			i = Character.getNumericValue(bar.charAt(2)) *(12) +i;
		}else
			i = Character.getNumericValue(bar.charAt(1)) *(12) +i;
		return (440 * Math.pow(2,((i-69.0)/12)));
	}

	public static void play(boolean child){
		Scanner scanner = new Scanner(notes);
		int i = 0;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			long time = 0;
			long request = 10000;
			long timep=0;
			while(timep<request){
				time=System.currentTimeMillis();
				  if(line.startsWith("beep ")){
						  i = freqToKey(line);
						  String[] parts = line.split(" ");
						  request = Long.valueOf(parts[4]);
						  if(child)
							  request = 200;
						  if(i>0 && timep==0){
							  player.play(i+"s-");
						  }
				  }else if(line.startsWith("sleep")){
					  try {
						  request=Long.valueOf(line.substring(6));
						  if(child)
							  request = 200;
						  Thread.sleep(request);
					} catch (NumberFormatException | InterruptedException e) {
						e.printStackTrace();
					}
					  
				  }else{
					  JOptionPane.showMessageDialog(new JFrame(), "Make sure the reader only contains beeps and sleeps.");
				  }
				  timep += System.currentTimeMillis() - time;
				  if(timep>=request)
					  player.play(i+"-s");
				}
		}
		scanner.close();
	}

	private static int freqToKey(String line) {
		String[] parts = line.split(" "); 
		double freq = Double.parseDouble(parts[2]);
		return (int) (12*(Math.log(freq/440)/Math.log(2))+69);
	}

	public static void read(){
		
		readInit=true;
		winReader.setVisible(true);
		winReader.setSize(300,600);
		winReader.setResizable(false);
		winReader.setTitle("Javkovsky Reader");
		winReader.setLayout(new BorderLayout());
		
		final JCheckBox child = new JCheckBox("Sad Child mode (legacy)");
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		FileDialog fDialog = new FileDialog(new JFrame(), "Save", FileDialog.SAVE);
                fDialog.setVisible(true);
                if(fDialog.getDirectory()!=null && fDialog.getFile()!=null)
	        		try {
		        		FileWriter recorder = new FileWriter(new File(fDialog.getDirectory() + fDialog.getFile()+".sh"));
							recorder.write(notes);
							recorder.close();
					} catch (IOException e) {
							e.printStackTrace();
					}
        	}
		});
		JButton load = new JButton("Load");
		load.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		FileDialog fDialog = new FileDialog(new JFrame(), "Load", FileDialog.LOAD);
                fDialog.setVisible(true);
        		StringBuilder sb = new StringBuilder();
        		if(fDialog.getDirectory()!=null && fDialog.getFile()!=null){
        			BufferedReader br;
					try {
						br = new BufferedReader(new FileReader(new File(fDialog.getDirectory() + fDialog.getFile())));
	        			if(fDialog.getFile().endsWith(".sh")){
			                    String textLine;
			                    while ((textLine = br.readLine()) != null) {
			                        sb.append(textLine);
			                        sb.append('\n');
			                    }
			                    br.close();
			                    if (sb.length() ==  0)
			                        sb.append("\n");
	        			}
	        			else if(fDialog.getFile().endsWith(".abc"))
	        				sb.append(abcfugue.getBeep(br));
	        			else
	        				JOptionPane.showMessageDialog(new JFrame(), "File extension not recognised.");
					} catch (IOException e) {
						e.printStackTrace();
					}
                notes = sb.toString();
                text.setText(notes);
        		}
        	}
		});
		JButton play = new JButton("Play");
		play.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		play(child.isSelected());
        	}
		});
		text.getDocument().addDocumentListener(new DocumentListener() {

	        public void removeUpdate(DocumentEvent e) {
	        	notes = text.getText();
	        }
	        
	        public void insertUpdate(DocumentEvent e) {
	        	notes = text.getText();
	        }

			public void changedUpdate(DocumentEvent e){}     
			
		});
		
		winReader.add(new JScrollPane(text), BorderLayout.CENTER);
		JPanel controls = new JPanel();
		JPanel buttons = new JPanel();
		controls.setLayout(new BorderLayout());
		controls.add(child, BorderLayout.NORTH);
		buttons.add(play);
		buttons.add(save);
		buttons.add(load);
		controls.add(buttons, BorderLayout.SOUTH);
		winReader.add(controls, BorderLayout.SOUTH);
	}
}
