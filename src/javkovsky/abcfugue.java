package src.javkovsky;

import java.io.BufferedReader;
import java.io.IOException;

public class abcfugue {

	public static String getBeep(BufferedReader br) {
		String foo = "";
		try {
			String line;
			while ((line = br.readLine()) != null) {
				if(line.length()>2)
				if(line.charAt(0) != '%' && line.charAt(1) != ':'){
					for(int i = 0;i<line.length();i++){
						if("abcdefg".contains(String.valueOf(Character.toLowerCase(line.charAt(i))))){
							String l = findLength(line, i);
							foo += "beep -f " + Application.getFreq(Character.toString(line.charAt(i))) + " -l " + l +"\n";
						}else if(line.charAt(i)=='z'){
							String l = findLength(line, i);
							foo += "sleep " + l +"\n";
						}
					}
					
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return foo;
	}

	private static String findLength(String line, int i) {
		String l = "";
		String upper = "";
		String lower = "";
		boolean frac = false;
		for(int j = i+1; j < line.length(); j++){
			char len = line.charAt(j);
			if("/1234567890".contains(String.valueOf(Character.toLowerCase(len)))){
				if(len=='/'){
					frac = true;
				}
				else if(!frac)
					upper+=len;
				else if(frac)
					lower+=len;
			}else if(len!=','&&len!='^'){
				break;
			}
		}
			
			if(lower==""&&upper=="")
				l = "200";
			else if(upper=="")
				l= String.valueOf((int)(200 * 1/Double.valueOf(lower)));
			else if(lower=="")
				l= String.valueOf((int)(200 * Double.valueOf(upper)));
			else
				l= String.valueOf((int)(200 * Double.valueOf(upper)/Double.valueOf(lower)));
	//		l = String.valueOf(500 * Double.valueOf(l));
		
		return l;
	}

}
